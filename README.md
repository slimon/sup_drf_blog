# sup_drf_blog

## Описание
Практическое занятие #12: Разработка API блога с использованием DRF
## Задача
Создать API для управления статьями в блоге с помощью Django REST Framework.

## Доступные Users:
Имеются два пользователя:
1) SuperUser - username: admin; password: admin 
2) Обычный пользователь - username: user1; password: user1

## Доступные URLы:
Для доступа в админ панель: [admin/](http://127.0.0.1:8000/admin/)

Просмотр всех постов с возможность создания нового постав: [api/posts](http://127.0.0.1:8000/api/posts/)

Просмотр деталей определенного поста: [api/posts/<int:pk>/](http://127.0.0.1:8000/api/posts/<int:pk>/)

Просмотр комментариев определенного поста: [api/posts/int:post_id/comments/](http://127.0.0.1:8000/api/posts/<int:post_id>/comments/)

Просмотр деталей определенного комментария: [api/comments/<int:pk>/](http://127.0.0.1:8000/api/comments/<int:pk>/)

Просмотр информации о пользователе: [api/users/<int:pk>/](http://127.0.0.1:8000/api/users/<int:pk>/)

## Порядок установки:
1. Установить зависимости из файла requirements.txt
```
pip install -r requirements.txt
```
2. Загрузить дамп
```
./manage.py loaddata fixtures.json
```
3. Применить миграцию:
```
./manage.py migrate
```
4. Запустить сервер:
```
./manage.py runserver
```
