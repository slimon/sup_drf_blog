from rest_framework import permissions


class IsAdminOrAuthenticatedOrOwnerOrReadOnly(permissions.BasePermission):
    def has_permission(self, request, view):
        if request.user and request.user.is_staff:
            return True
        elif request.method in permissions.SAFE_METHODS or request.user and request.user.is_authenticated:
            return True

    def has_object_permission(self, request, view, obj):
        if request.user.is_staff:
            return True
        elif request.method in permissions.SAFE_METHODS:
            return True

        return obj.user == request.user
