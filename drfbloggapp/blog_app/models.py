from django.db import models
from django.contrib.auth.models import User


class Post(models.Model):
    title = models.CharField(max_length=200)
    description = models.TextField(blank=True, null=True)
    created_at = models.DateTimeField(auto_now_add=True)
    user = models.ForeignKey(User, verbose_name='User', on_delete=models.PROTECT)

    def __str__(self):
        return self.title


class Comment(models.Model):
    body = models.CharField(max_length=300)
    user = models.ForeignKey(User, verbose_name='User', on_delete=models.PROTECT)
    post = models.ForeignKey(Post, on_delete=models.PROTECT, related_name='comments', verbose_name='Post')
    created_at = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.body
